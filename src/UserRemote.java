import javax.ejb.Remote;

@Remote
public interface UserRemote {
    User create(User user);
    User read(Integer id);
    void update(User user);
    void delete(Integer id);
}
