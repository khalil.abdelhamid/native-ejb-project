import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = "USER")
public class UserImpl implements UserRemote, UserLocal{

    @PersistenceContext(unitName = "NewPersistenceUnit")
    private EntityManager em;

    @Override
    public User create(User user) {
        em.persist(user);
        return user;
    }

    @Override
    public User read(Integer id) {
        User found = em.find(User.class, id);
        if ( found == null ) throw new RuntimeException("user not found with id " + id);
        return found;
    }

    @Override
    public void update(User user) {
        em.persist(user);
    }

    @Override
    public void delete(Integer id) {
    }
}
