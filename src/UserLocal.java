import javax.ejb.Local;

@Local
public interface UserLocal {
    User create(User user);
    User read(Integer id);
    void update(User user);
    void delete(Integer id);
}
